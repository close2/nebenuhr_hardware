EESchema Schematic File Version 2
LIBS:power
LIBS:d1_nebenuhr_shield-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Nebenuhr"
Date "2018-06-23"
Rev "2.0"
Comp "Christian Loitsch"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Label 7250 1850 2    60   ~ 0
+5V
Text Label 7250 1950 2    60   ~ 0
GND
Text Label 7250 2050 2    60   ~ 0
D4
Text Label 7250 2150 2    60   ~ 0
D3
Text Label 7250 2250 2    60   ~ 0
D2
Text Label 7250 2350 2    60   ~ 0
D1
Text Label 7250 2450 2    60   ~ 0
RX
Text Label 7250 2550 2    60   ~ 0
TX
Text Label 8100 1850 0    60   ~ 0
+3.3V
Text Label 8100 1950 0    60   ~ 0
D8
Text Label 8100 2050 0    60   ~ 0
D7
Text Label 8100 2150 0    60   ~ 0
D6
Text Label 8100 2250 0    60   ~ 0
D5
Text Label 8100 2350 0    60   ~ 0
D0
Text Label 8100 2450 0    60   ~ 0
A0
Text Label 8100 2550 0    60   ~ 0
RST
Text Notes 7000 2050 2    60   ~ 0
GPIO2
Text Notes 7000 2150 2    60   ~ 0
GPIO0
Text Notes 7000 2250 2    60   ~ 0
GPIO4
Text Notes 7000 2350 2    60   ~ 0
GPIO5
Text Notes 8450 1950 0    60   ~ 0
GPIO15
Text Notes 8450 2050 0    60   ~ 0
GPIO13
Text Notes 8450 2150 0    60   ~ 0
GPIO12
Text Notes 8450 2250 0    60   ~ 0
GPIO14
Text Notes 8450 2350 0    60   ~ 0
GPIO16
Text Notes 6350 1400 0    60   ~ 0
D1 Mini Shield
$Comp
L R R7
U 1 1 5B24C96F
P 8200 5200
F 0 "R7" V 8280 5200 50  0000 C CNN
F 1 "R180" V 8200 5200 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 8130 5200 50  0001 C CNN
F 3 "" H 8200 5200 50  0001 C CNN
	1    8200 5200
	1    0    0    -1  
$EndComp
$Comp
L R R8
U 1 1 5B24CA27
P 10450 5200
F 0 "R8" V 10530 5200 50  0000 C CNN
F 1 "R180" V 10450 5200 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 10380 5200 50  0001 C CNN
F 3 "" H 10450 5200 50  0001 C CNN
	1    10450 5200
	1    0    0    -1  
$EndComp
$Comp
L R R2
U 1 1 5B24CA82
P 2300 6500
F 0 "R2" V 2400 6500 50  0000 C CNN
F 1 "R1k" V 2300 6500 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 2230 6500 50  0001 C CNN
F 3 "" H 2300 6500 50  0001 C CNN
	1    2300 6500
	0    1    1    0   
$EndComp
$Comp
L R R6
U 1 1 5B24CAB3
P 4800 6500
F 0 "R6" V 4700 6500 50  0000 C CNN
F 1 "R1k" V 4800 6500 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 4730 6500 50  0001 C CNN
F 3 "" H 4800 6500 50  0001 C CNN
	1    4800 6500
	0    -1   -1   0   
$EndComp
$Comp
L R R1
U 1 1 5B24CAD4
P 1900 6000
F 0 "R1" V 1980 6000 50  0000 C CNN
F 1 "R1k" V 1900 6000 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 1830 6000 50  0001 C CNN
F 3 "" H 1900 6000 50  0001 C CNN
	1    1900 6000
	-1   0    0    1   
$EndComp
$Comp
L R R5
U 1 1 5B24CF7D
P 4400 6000
F 0 "R5" V 4480 6000 50  0000 C CNN
F 1 "R1k" V 4400 6000 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 4330 6000 50  0001 C CNN
F 3 "" H 4400 6000 50  0001 C CNN
	1    4400 6000
	1    0    0    -1  
$EndComp
$Comp
L DMHC3025LSD-13 Q2
U 1 1 5B250CEF
P 3500 4550
F 0 "Q2" H 3200 4950 60  0000 C CNN
F 1 "DMHC3025LSD-13" V 3800 4150 60  0000 L CNN
F 2 "digikey-footprints:SOIC-8_W3.9mm" V 3700 4750 60  0001 L CNN
F 3 "https://www.diodes.com/assets/Datasheets/DMHC3025LSD.pdf" H 3700 4850 60  0001 L CNN
F 4 "DMHC3025LSD-13DICT-ND" H 3700 4950 60  0001 L CNN "Digi-Key_PN"
F 5 "DMHC3025LSD-13" H 3700 5050 60  0001 L CNN "MPN"
F 6 "Discrete Semiconductor Products" H 3700 5150 60  0001 L CNN "Category"
F 7 "Transistors - FETs, MOSFETs - Arrays" H 3700 5250 60  0001 L CNN "Family"
F 8 "https://www.diodes.com/assets/Datasheets/DMHC3025LSD.pdf" H 3700 5350 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/diodes-incorporated/DMHC3025LSD-13/DMHC3025LSD-13DICT-ND/3677986" H 3700 5450 60  0001 L CNN "DK_Detail_Page"
F 10 "MOSFET 2N/2P-CH 30V 8SO" H 3700 5550 60  0001 L CNN "Description"
F 11 "Diodes Incorporated" H 3700 5650 60  0001 L CNN "Manufacturer"
F 12 "Active" H 3700 5750 60  0001 L CNN "Status"
	1    3500 4550
	1    0    0    -1  
$EndComp
$Comp
L LED D1
U 1 1 5B251B07
P 8200 5500
F 0 "D1" H 8200 5600 50  0000 C CNN
F 1 "LED" H 8200 5400 50  0000 C CNN
F 2 "LEDs:LED_1206_HandSoldering" H 8200 5500 50  0001 C CNN
F 3 "" H 8200 5500 50  0001 C CNN
	1    8200 5500
	0    -1   -1   0   
$EndComp
$Comp
L LED D2
U 1 1 5B251BAF
P 10450 5500
F 0 "D2" H 10450 5600 50  0000 C CNN
F 1 "LED" H 10450 5400 50  0000 C CNN
F 2 "LEDs:LED_1206_HandSoldering" H 10450 5500 50  0001 C CNN
F 3 "" H 10450 5500 50  0001 C CNN
	1    10450 5500
	0    -1   -1   0   
$EndComp
$Comp
L Screw_Terminal_01x02 J3
U 1 1 5B255C67
P 2700 4850
F 0 "J3" H 2800 4800 50  0000 C CNN
F 1 "Screw_Terminal_01x02" H 2700 4950 50  0000 C CNN
F 2 "D1_mini:2pin_screw_terminal" H 2700 4850 50  0001 C CNN
F 3 "" H 2700 4850 50  0001 C CNN
	1    2700 4850
	-1   0    0    1   
$EndComp
$Comp
L Screw_Terminal_01x01 J1
U 1 1 5B255DFA
P 2600 2000
F 0 "J1" H 2600 2100 50  0000 C CNN
F 1 "Screw_Terminal_01x01" V 2700 1550 50  0000 C CNN
F 2 "D1_mini:2pin_screw_terminal" H 2600 2000 50  0001 C CNN
F 3 "" H 2600 2000 50  0001 C CNN
	1    2600 2000
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR01
U 1 1 5B257182
P 3600 5100
F 0 "#PWR01" H 3600 4850 50  0001 C CNN
F 1 "GND" H 3600 4950 50  0000 C CNN
F 2 "" H 3600 5100 50  0001 C CNN
F 3 "" H 3600 5100 50  0001 C CNN
	1    3600 5100
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG02
U 1 1 5B2573BF
P 3050 2250
F 0 "#FLG02" H 3050 2325 50  0001 C CNN
F 1 "PWR_FLAG" H 3050 2400 50  0000 C CNN
F 2 "" H 3050 2250 50  0001 C CNN
F 3 "" H 3050 2250 50  0001 C CNN
	1    3050 2250
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG03
U 1 1 5B257602
P 6350 1750
F 0 "#FLG03" H 6350 1825 50  0001 C CNN
F 1 "PWR_FLAG" H 6350 1900 50  0000 C CNN
F 2 "" H 6350 1750 50  0001 C CNN
F 3 "" H 6350 1750 50  0001 C CNN
	1    6350 1750
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG04
U 1 1 5B257689
P 6350 2000
F 0 "#FLG04" H 6350 2075 50  0001 C CNN
F 1 "PWR_FLAG" H 6350 2150 50  0000 C CNN
F 2 "" H 6350 2000 50  0001 C CNN
F 3 "" H 6350 2000 50  0001 C CNN
	1    6350 2000
	-1   0    0    1   
$EndComp
$Comp
L PWR_FLAG #FLG05
U 1 1 5B2577ED
P 8100 1750
F 0 "#FLG05" H 8100 1825 50  0001 C CNN
F 1 "PWR_FLAG" H 8100 1900 50  0000 C CNN
F 2 "" H 8100 1750 50  0001 C CNN
F 3 "" H 8100 1750 50  0001 C CNN
	1    8100 1750
	1    0    0    -1  
$EndComp
NoConn ~ 7250 2050
NoConn ~ 7250 2150
NoConn ~ 7250 2450
NoConn ~ 7250 2550
NoConn ~ 8100 2550
NoConn ~ 8100 2450
NoConn ~ 8100 2350
$Comp
L GND #PWR06
U 1 1 5B25891C
P 9700 4400
F 0 "#PWR06" H 9700 4150 50  0001 C CNN
F 1 "GND" H 9700 4250 50  0000 C CNN
F 2 "" H 9700 4400 50  0001 C CNN
F 3 "" H 9700 4400 50  0001 C CNN
	1    9700 4400
	1    0    0    -1  
$EndComp
Text GLabel 3000 4550 0    60   Input ~ 0
H-Bridge-B
Text GLabel 3000 4350 0    60   Input ~ 0
H-Bridge-A
Text GLabel 4500 6200 2    60   Output ~ 0
H-Bridge-B
Text GLabel 3000 4650 0    60   Input ~ 0
H-Bridge-A
Text GLabel 3000 4450 0    60   Input ~ 0
H-Bridge-B
Text GLabel 2000 6250 2    60   Output ~ 0
H-Bridge-A
Text GLabel 2450 6500 2    60   Input ~ 0
H-Bridge-A-Signal
Text GLabel 4950 6500 2    60   Input ~ 0
H-Bridge-B-Signal
Text GLabel 6250 2350 0    60   Output ~ 0
H-Bridge-B-Signal
Text GLabel 8950 2150 2    60   Output ~ 0
H-Bridge-A-Signal
$Comp
L GND #PWR07
U 1 1 5B25C8A2
P 1900 6750
F 0 "#PWR07" H 1900 6500 50  0001 C CNN
F 1 "GND" H 1900 6600 50  0000 C CNN
F 2 "" H 1900 6750 50  0001 C CNN
F 3 "" H 1900 6750 50  0001 C CNN
	1    1900 6750
	1    0    0    -1  
$EndComp
Text GLabel 6250 2250 0    60   Input ~ 0
Factory-Reset-Button
Text GLabel 8850 4400 0    60   Output ~ 0
Factory-Reset-Button
Text GLabel 8950 1950 2    60   Output ~ 0
Pause-On-Signal
Text GLabel 7600 5850 0    60   Input ~ 0
Pause-On-Signal
$Comp
L GND #PWR08
U 1 1 5B25D733
P 8200 6050
F 0 "#PWR08" H 8200 5800 50  0001 C CNN
F 1 "GND" H 8200 5900 50  0000 C CNN
F 2 "" H 8200 6050 50  0001 C CNN
F 3 "" H 8200 6050 50  0001 C CNN
	1    8200 6050
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR09
U 1 1 5B25D91C
P 10450 6050
F 0 "#PWR09" H 10450 5800 50  0001 C CNN
F 1 "GND" H 10450 5900 50  0000 C CNN
F 2 "" H 10450 6050 50  0001 C CNN
F 3 "" H 10450 6050 50  0001 C CNN
	1    10450 6050
	1    0    0    -1  
$EndComp
Text GLabel 9850 5850 0    60   Input ~ 0
Pause-Off-Signal
Text GLabel 8950 2050 2    60   Output ~ 0
Pause-Off-Signal
$Comp
L GND #PWR010
U 1 1 5B25E2E5
P 4400 6750
F 0 "#PWR010" H 4400 6500 50  0001 C CNN
F 1 "GND" H 4400 6600 50  0000 C CNN
F 2 "" H 4400 6750 50  0001 C CNN
F 3 "" H 4400 6750 50  0001 C CNN
	1    4400 6750
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR011
U 1 1 5B25F6F2
P 6650 2000
F 0 "#PWR011" H 6650 1750 50  0001 C CNN
F 1 "GND" H 6650 1850 50  0000 C CNN
F 2 "" H 6650 2000 50  0001 C CNN
F 3 "" H 6650 2000 50  0001 C CNN
	1    6650 2000
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR012
U 1 1 5B264610
P 6650 1750
F 0 "#PWR012" H 6650 1600 50  0001 C CNN
F 1 "+5V" H 6650 1890 50  0000 C CNN
F 2 "" H 6650 1750 50  0001 C CNN
F 3 "" H 6650 1750 50  0001 C CNN
	1    6650 1750
	1    0    0    -1  
$EndComp
Text Notes 2000 1750 0    60   ~ 0
Connections to Boost Converter
Text Notes 2350 3600 0    60   ~ 0
H-Bridge and H-Bridge Signal 3.3V to 18V amplifier
$Comp
L +18V #PWR013
U 1 1 5B265957
P 1900 5850
F 0 "#PWR013" H 1900 5700 50  0001 C CNN
F 1 "+18V" H 1900 5990 50  0000 C CNN
F 2 "" H 1900 5850 50  0001 C CNN
F 3 "" H 1900 5850 50  0001 C CNN
	1    1900 5850
	1    0    0    -1  
$EndComp
$Comp
L +18V #PWR014
U 1 1 5B265A11
P 4400 5850
F 0 "#PWR014" H 4400 5700 50  0001 C CNN
F 1 "+18V" H 4400 5990 50  0000 C CNN
F 2 "" H 4400 5850 50  0001 C CNN
F 3 "" H 4400 5850 50  0001 C CNN
	1    4400 5850
	1    0    0    -1  
$EndComp
$Comp
L +18V #PWR015
U 1 1 5B265A68
P 3500 4100
F 0 "#PWR015" H 3500 3950 50  0001 C CNN
F 1 "+18V" H 3500 4240 50  0000 C CNN
F 2 "" H 3500 4100 50  0001 C CNN
F 3 "" H 3500 4100 50  0001 C CNN
	1    3500 4100
	1    0    0    -1  
$EndComp
$Comp
L +18V #PWR016
U 1 1 5B265B75
P 3400 2250
F 0 "#PWR016" H 3400 2100 50  0001 C CNN
F 1 "+18V" H 3400 2390 50  0000 C CNN
F 2 "" H 3400 2250 50  0001 C CNN
F 3 "" H 3400 2250 50  0001 C CNN
	1    3400 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 4750 3100 4750
Wire Wire Line
	2600 2500 2600 2600
Wire Wire Line
	3500 4100 3500 4150
Wire Wire Line
	1900 6150 1900 6300
Wire Wire Line
	4400 6150 4400 6300
Wire Wire Line
	3100 5750 3100 5750
Connection ~ 1900 6250
Connection ~ 4400 6200
Wire Wire Line
	1900 6700 1900 6750
Wire Wire Line
	3450 6500 3450 6500
Wire Wire Line
	3100 4850 2900 4850
Wire Wire Line
	3600 5050 3600 5100
Wire Wire Line
	3400 2250 3400 2600
Wire Wire Line
	3400 2600 2600 2600
Connection ~ 2600 2600
Wire Wire Line
	3050 2250 3050 2600
Connection ~ 3050 2600
Wire Wire Line
	8100 1750 8100 1850
Wire Wire Line
	3100 4450 3000 4450
Wire Wire Line
	3100 4550 3000 4550
Wire Wire Line
	3000 4650 3100 4650
Wire Wire Line
	3100 4350 3000 4350
Wire Wire Line
	1900 6250 2000 6250
Wire Wire Line
	4400 6200 4500 6200
Wire Wire Line
	2200 6500 2150 6500
Wire Wire Line
	4700 6500 4650 6500
Wire Wire Line
	6250 2350 7250 2350
Wire Wire Line
	8850 4400 9000 4400
Wire Wire Line
	9400 4400 9700 4400
Wire Wire Line
	8100 1950 8950 1950
Wire Wire Line
	8100 2050 8950 2050
Wire Wire Line
	6350 1950 7250 1950
Wire Wire Line
	6350 2000 6350 1950
Connection ~ 6350 1950
Wire Wire Line
	6350 1750 6350 1850
Connection ~ 6350 1850
Wire Wire Line
	4400 6700 4400 6750
Wire Wire Line
	6650 1950 6650 2000
Connection ~ 6650 1950
Wire Wire Line
	6650 1750 6650 1850
Connection ~ 6650 1850
Wire Notes Line
	1650 1600 3850 1600
Wire Notes Line
	3850 1600 3850 2700
Wire Notes Line
	3850 2700 1650 2700
Wire Notes Line
	1650 2700 1650 1600
Wire Notes Line
	1000 3450 6400 3450
Wire Notes Line
	6400 3450 6400 7150
Wire Notes Line
	6400 7150 1000 7150
Wire Notes Line
	1000 7150 1000 3450
Wire Wire Line
	6250 2250 7250 2250
Wire Wire Line
	8100 2250 8950 2250
$Comp
L B3U-1000P S1
U 1 1 5B259CEC
P 9200 4400
F 0 "S1" H 9200 4560 60  0000 C CNN
F 1 "B3U-1000P" H 9200 4300 60  0000 C CNN
F 2 "Buttons_Switches_SMD:SW_SPST_EVPBF" H 9400 4600 60  0001 L CNN
F 3 "http://omronfs.omron.com/en_US/ecb/products/pdf/en-b3u.pdf" H 9400 4700 60  0001 L CNN
F 4 "SW1020CT-ND" H 9400 4800 60  0001 L CNN "Digi-Key_PN"
F 5 "B3U-1000P" H 9400 4900 60  0001 L CNN "MPN"
F 6 "Switches" H 9400 5000 60  0001 L CNN "Category"
F 7 "Tactile Switches" H 9400 5100 60  0001 L CNN "Family"
F 8 "http://omronfs.omron.com/en_US/ecb/products/pdf/en-b3u.pdf" H 9400 5200 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/omron-electronics-inc-emc-div/B3U-1000P/SW1020CT-ND/1534357" H 9400 5300 60  0001 L CNN "DK_Detail_Page"
F 10 "SWITCH TACTILE SPST-NO 0.05A 12V" H 9400 5400 60  0001 L CNN "Description"
F 11 "Omron Electronics Inc-EMC Div" H 9400 5500 60  0001 L CNN "Manufacturer"
F 12 "Active" H 9400 5600 60  0001 L CNN "Status"
	1    9200 4400
	1    0    0    -1  
$EndComp
Text GLabel 8750 3700 0    60   Output ~ 0
Pause-Button
Text GLabel 8950 2250 2    60   Input ~ 0
Pause-Button
Wire Wire Line
	8100 2150 8950 2150
$Comp
L Screw_Terminal_01x02 J5
U 1 1 5B2E8C2F
P 9400 3800
F 0 "J5" H 9500 3750 50  0000 C CNN
F 1 "Screw_Terminal_01x02" H 9400 3900 50  0000 C CNN
F 2 "D1_mini:2pin_screw_terminal" H 9400 3800 50  0001 C CNN
F 3 "" H 9400 3800 50  0001 C CNN
	1    9400 3800
	1    0    0    1   
$EndComp
$Comp
L GND #PWR017
U 1 1 5B2E8F45
P 8750 3800
F 0 "#PWR017" H 8750 3550 50  0001 C CNN
F 1 "GND" H 8750 3650 50  0000 C CNN
F 2 "" H 8750 3800 50  0001 C CNN
F 3 "" H 8750 3800 50  0001 C CNN
	1    8750 3800
	-1   0    0    -1  
$EndComp
Wire Wire Line
	9200 3800 8750 3800
Wire Wire Line
	9200 3700 8750 3700
$Comp
L BC817 Q4
U 1 1 5B44F601
P 8100 5850
F 0 "Q4" H 8300 5925 50  0000 L CNN
F 1 "BC817" H 8300 5850 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23_Handsoldering" H 8300 5775 50  0001 L CIN
F 3 "" H 8100 5850 50  0001 L CNN
	1    8100 5850
	1    0    0    -1  
$EndComp
$Comp
L BC817 Q5
U 1 1 5B44F690
P 10350 5850
F 0 "Q5" H 10550 5925 50  0000 L CNN
F 1 "BC817" H 10550 5850 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23_Handsoldering" H 10550 5775 50  0001 L CIN
F 3 "" H 10350 5850 50  0001 L CNN
	1    10350 5850
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR018
U 1 1 5B44F77A
P 8200 5050
F 0 "#PWR018" H 8200 4900 50  0001 C CNN
F 1 "+5V" H 8200 5190 50  0000 C CNN
F 2 "" H 8200 5050 50  0001 C CNN
F 3 "" H 8200 5050 50  0001 C CNN
	1    8200 5050
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR019
U 1 1 5B44F826
P 10450 5050
F 0 "#PWR019" H 10450 4900 50  0001 C CNN
F 1 "+5V" H 10450 5190 50  0000 C CNN
F 2 "" H 10450 5050 50  0001 C CNN
F 3 "" H 10450 5050 50  0001 C CNN
	1    10450 5050
	1    0    0    -1  
$EndComp
$Comp
L R R9
U 1 1 5B44FC5B
P 7750 5850
F 0 "R9" V 7830 5850 50  0000 C CNN
F 1 "R1k" V 7750 5850 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 7680 5850 50  0001 C CNN
F 3 "" H 7750 5850 50  0001 C CNN
	1    7750 5850
	0    1    1    0   
$EndComp
$Comp
L R R10
U 1 1 5B44FEB6
P 10000 5850
F 0 "R10" V 10080 5850 50  0000 C CNN
F 1 "R1k" V 10000 5850 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 9930 5850 50  0001 C CNN
F 3 "" H 10000 5850 50  0001 C CNN
	1    10000 5850
	0    1    1    0   
$EndComp
$Comp
L Conn_01x08 J6
U 1 1 5B450B6D
P 7450 2150
F 0 "J6" H 7450 2550 50  0000 C CNN
F 1 "Conn_01x08" H 7450 1650 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x08_Pitch2.54mm" H 7450 2150 50  0001 C CNN
F 3 "" H 7450 2150 50  0001 C CNN
	1    7450 2150
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x08 J7
U 1 1 5B450C78
P 7900 2150
F 0 "J7" H 7900 2550 50  0000 C CNN
F 1 "Conn_01x08" H 7900 1650 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x08_Pitch2.54mm" H 7900 2150 50  0001 C CNN
F 3 "" H 7900 2150 50  0001 C CNN
	1    7900 2150
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6350 1850 7250 1850
$Comp
L BC817 Q1
U 1 1 5B6962D4
P 2000 6500
F 0 "Q1" H 2200 6575 50  0000 L CNN
F 1 "BC817" H 2200 6500 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23_Handsoldering" H 2200 6425 50  0001 L CIN
F 3 "" H 2000 6500 50  0001 L CNN
	1    2000 6500
	-1   0    0    -1  
$EndComp
$Comp
L BC817 Q3
U 1 1 5B696399
P 4500 6500
F 0 "Q3" H 4700 6575 50  0000 L CNN
F 1 "BC817" H 4700 6500 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23_Handsoldering" H 4700 6425 50  0001 L CIN
F 3 "" H 4500 6500 50  0001 L CNN
	1    4500 6500
	-1   0    0    -1  
$EndComp
$Comp
L R R3
U 1 1 5B69FBDE
P 2600 2350
F 0 "R3" V 2680 2350 50  0000 C CNN
F 1 "R10" V 2600 2350 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 2530 2350 50  0001 C CNN
F 3 "" H 2600 2350 50  0001 C CNN
	1    2600 2350
	1    0    0    -1  
$EndComp
$EndSCHEMATC
