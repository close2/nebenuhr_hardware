## Hardware for nebenuhr shield.

See my other project (nebenuhr) for the software.

## Bugs

You should add 1 kOhm resistor between D1 and GND and between D6 and GND.

Also note that I incorrectly assumed that the reset button of the D1 is on
the right (when viewed from the top).

You won't be able to press the reset button unless you stack the shield onto
the bottom of a D1.

![](images/d1_nebenuhr_shield.png)
